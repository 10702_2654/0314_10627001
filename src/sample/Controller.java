package sample;

import javafx.event.ActionEvent;
import javafx.scene.control.Button;
import javafx.scene.control.Label;

public class Controller {
    public Label monitor;
    public Button disone;
    public Button disTwo;
    public Button disThree;
    public Button disFour;
    public Button disFive;
    public Button disSix;
    public Button disSeven;
    public Button disEight;
    public Button disNine;
    public Button disAdd;
    public Button disMul;
    public Button disExc;
    public Button disCut;
    public Button disEqual;
    int buf=0, num=0;


    public void doClick(ActionEvent actionEvent) {
        Button button = (Button) actionEvent.getSource();
        String number = monitor.getText();
        monitor.setText(number.concat(button.getText()));
    }

    public void ClickSymbel(ActionEvent actionEvent) {
        Button button = (Button) actionEvent.getSource();
        switch (button.getText()) {
            case "+":
                buf = Integer.parseInt(monitor.getText());
                monitor.setText("");
                break;

            case "=":
                num = Integer.parseInt(monitor.getText());
                monitor.setText(String.valueOf(buf + num));
                break;

        }
    }
}
