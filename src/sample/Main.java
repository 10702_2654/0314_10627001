package sample;

import javafx.application.Application;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.layout.FlowPane;
import javafx.scene.layout.StackPane;
import javafx.stage.Stage;

public class Main extends Application {

    @Override
    public void start(Stage primaryStage) throws Exception{
        Parent root = FXMLLoader.load(getClass().getResource("sample.fxml"));


    /*FlowPane root = new FlowPane();
    Button button = new Button("OK");
    Button btnCancel = new Button("Cancel");
    Button btnAdd = new Button("+");
    Button btnCut = new Button("-");
    Button btnMultiply = new Button("*");
    Button btnExcept = new Button("/");
    Button btnEqual = new Button("=");



    button.setOnAction(new EventHandler<ActionEvent>() {
        @Override
        public void handle(ActionEvent event) {
            System.out.println("OK");
        }
    });
    btnCancel.setOnAction(new EventHandler<ActionEvent>() {
        @Override
        public void handle(ActionEvent event) {
            System.out.println("Cancel");
        }

    });

    btnAdd.setOnAction(new EventHandler<ActionEvent>() {
        @Override
        public void handle(ActionEvent event) {
            System.out.println("+");
        }
    });

    btnCut.setOnAction(new EventHandler<ActionEvent>() {
        @Override
        public void handle(ActionEvent event) {
            System.out.println("-");
        }
    });

    btnMultiply.setOnAction(new EventHandler<ActionEvent>() {
        @Override
        public void handle(ActionEvent event) {
            System.out.println("*");
        }
    });

    btnExcept.setOnAction(new EventHandler<ActionEvent>() {
        @Override
        public void handle(ActionEvent event) {
            System.out.println("/");
        }
    });

    btnEqual.setOnAction(new EventHandler<ActionEvent>() {
        @Override
        public void handle(ActionEvent event) {
            System.out.println("=");
        }
    });


    root.getChildren().addAll(button, btnCancel, btnAdd, btnCut, btnMultiply, btnExcept, btnEqual);
    */

    primaryStage.setTitle("My Calculator");
    primaryStage.setScene(new Scene(root, 400, 250));
    primaryStage.show();
}
    public static void main(String[] args) {
        launch(args);
    }
}
